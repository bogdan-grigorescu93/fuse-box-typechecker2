# my-fuse-box-typechecker
I made some changes to github.com/fuse-box/fuse-box-typechecker.

### How to install
``` npm  git+https://gitlab.com/bogdan-grigorescu93/fuse-box-typechecker2.git --save-dev ```


### Note
This have been tested with
 * "tslint": "^5.4.3",
 * "typescript": "^2.4.1"

So this might not work with earlier version if typescript and tslint (tsLint 3 will fail, been tested).


### How to use
```javascript

/* the only difference is that runAsync will resolve the promise 
 * with the found errors.
 * This way you can send the errors to the client
 */

app.watch()
    .hmr({ reload: false })
    .completed((proc) => {
        const test = typechecker.runPromise();
        test.then((groupedErrors) => {

            let errors = [];
            for (key in groupedErrors) {
                errors = errors.concat(groupedErrors[key]);
            }

            if (errors.length === 0) {
                proc.bundle.clearErrors();
            } else {
                errors.forEach((e) => {
                    proc.bundle.addError(JSON.stringify(e));
                });
            }
        });
    });

```

import { IInternalTypeCheckerOptions, ITSLintError, ITSError } from './interfaces';
export declare class Checker {
    private options;
    private program;
    private elapsedInspectionTime;
    private tsDiagnostics;
    private lintFileResult;
    constructor();
    inspectCode(options: IInternalTypeCheckerOptions): void;
    printResult(isWorker?: boolean): {
        [k: string]: (ITSLintError | ITSError)[];
    };
    private writeText(text);
    private processLintFiles();
    private processTsDiagnostics();
}
